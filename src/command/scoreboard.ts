import {Message} from "discord.js";
import * as rp from "request-promise";
import {Command} from "./command";

class ScoreboardCommandClass extends Command {

    hidden(): boolean {
        return true;
    }

    async handleCommand(args: string[], msg: Message): Promise<string> {
        let reqOptions = {
            uri: "https://redditball.xyz/api/v1/games/3/18",
            json: true,
        };

        let games = await rp(reqOptions);

        let out = "```\n#Playoffs\n\nTEAMS|SCORE|INNING|BASES\n:--|:-:|:-:|:-:\n";

        let playoffGames = games.filter(t => t.homeTeam.tag === "COL" || t.homeTeam.tag === "TEX");
        let losersGames = games.filter(t => t.homeTeam.tag !== "COL" && t.homeTeam.tag !== "TEX");
        for (let game of playoffGames) {
            out += `[${game.awayTeam.tag} @ ${game.homeTeam.tag}](https://redditball.xyz/games/${game.id})|${game.awayScore} - ${game.homeScore}|${game.completed ? `FINAL` : `${game.inning}.${game.outs}`}|${game.thirdOccupied ? "x" : "-"} ^${game.secondOccupied ? "x" : "-"} ${game.firstOccupied ? "x" : "-"}\n`;
        }

        out += "\n#Summer Sadness\n\nTEAMS|SCORE|INNING|BASES\n:--|:-:|:-:|:-:\n";
        for (let game of losersGames) {
            out += `[${game.awayTeam.tag} @ ${game.homeTeam.tag}](https://redditball.xyz/games/${game.id})|${game.awayScore} - ${game.homeScore}|${game.completed ? `FINAL` : `${game.inning}.${game.outs}`}|${game.thirdOccupied ? "x" : "-"} ^${game.secondOccupied ? "x" : "-"} ${game.firstOccupied ? "x" : "-"}\n`;
        }

        out += "```";
        return out;
    }

}

export const ScoreboardCommand = new ScoreboardCommandClass();
