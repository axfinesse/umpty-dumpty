import * as Discord from "discord.js";
import {Message, RichEmbed, TextChannel} from "discord.js";
import * as rp from "request-promise";
import {ClaimCommand} from "./command/claim";
import {Command} from "./command/command";
import {DraftCommand, DraftTimerCommand} from "./command/draft";
import {HelpCommand} from "./command/help";
import {InviteCommand} from "./command/invite";
import {GameSheetCommand, HandbookCommand, RulebookCommand} from "./command/link-commands";
import {MyGamesCommand} from "./command/my-games";
import {
    Balk,
    Belle, Birds,
    CatGirl, F5,
    Franki,
    NLHelp, SexBot,
    Sneeze,
    SpaceRace,
    SpamNLCentral,
    SteveHarvey,
} from "./command/nl-central-shit";
import {PingCommand} from "./command/ping";
import {
    PingGMOptInCommand,
    PingGMOptOutCommand,
    PingOptInCommand,
    PingOptOutCommand,
    UmpPingBackOptInCommand,
    UmpPingBackOptOutCommand,
} from "./command/ping-optin-commands";
import {PlayerCommand} from "./command/player";
import {RangesCommand} from "./command/ranges";
import {RoleMeCommand} from "./command/roleme";
import {ScoreboardCommand} from "./command/scoreboard";
import {SwingCommand} from "./command/swing";
import {getBotSetting, setBotSetting} from "./database";
import {
    DEVELOPMENT_MODE,
    EMBED_FOOTER, SNOWFLAKE_CHANNEL_NL_CENTRAL, SNOWFLAKE_CHANNEL_OOTC, SNOWFLAKE_CHANNEL_SCOREBOARD,
    SNOWFLAKE_CHANNEL_UMP_PINGS,
    SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN,
    SNOWFLAKE_USER_LLAMOS,
} from "./important-constants";

const DISCORD_TOKEN = process.env.DISCORD_TOKEN;
const discordClient = new Discord.Client();
const prefix = ".";

// Log when we make a connection to Discord
discordClient.on("ready", async () => {
    if (!DEVELOPMENT_MODE) {
        // await updateScoreboard();
        await updateFCBScoreboard();
    }
    console.log("Logged in!");
});

const GMs = {
    ARI: "159484193947123712",
    ATL: "80385346734395392",
    BAL: "406898210804858884",
    BOS: "298191208776204288",
    CHC: "138184297797386240",
    CIN: "97154612594741248",
    CLE: "121733091034267648",
    COL: "378210771005341698",
    CWS: "231100460482691074",
    DET: "86858721061269504",
    HOU: "345191609098567680",
    KCR: "315706495473942529",
    LAA: "470202656992919553",
    LAD: "334203993607766017",
    MIA: "319330358636314634",
    MIL: "416272664756355075",
    MIN: "140306370359459840",
    MTL: "342097160122269698",
    NYM: "346750558901764117",
    NYY: "134845755042037761",
    OAK: "345339723323146240",
    PHI: "142425591382016000",
    PIT: "172167687143817217",
    SDP: "347623617456242690",
    SEA: "345455572599963648",
    SFG: "217027937252278272",
    STL: "86530663963983872",
    TBD: "401796234001514501",
    TEX: "370823935282905090",
    TOR: "144969605977341953",
};

// Maps the command names to the command instances
export const commandMap = {
    player: PlayerCommand,
    draft: DraftCommand,
    drafttimer: DraftTimerCommand,
    swing: SwingCommand,
    handbook: HandbookCommand,
    gamesheet: GameSheetCommand,
    rulebook: RulebookCommand,
    claim: ClaimCommand,
    ping: PingCommand,
    ranges: RangesCommand,
    mygames: MyGamesCommand,
    pingoptin: PingOptInCommand,
    pingoptout: PingOptOutCommand,
    gmpingoptin: PingGMOptInCommand,
    gmpingoptout: PingGMOptOutCommand,
    umppingoptout: UmpPingBackOptOutCommand,
    umppingoptin: UmpPingBackOptInCommand,
    invite: InviteCommand,
    scoreboard: ScoreboardCommand,
    roleme: RoleMeCommand,
    help: HelpCommand,
    // Here be NL Central
    nlhelp: NLHelp,
    franki: Franki,
    birds: Birds,
    nlcentral: SpamNLCentral,
    catgirl: CatGirl,
    steveharvey: SteveHarvey,
    sneeze: Sneeze,
    spacerace: SpaceRace,
    f5: F5,
    sexbot: SexBot,
    belle: Belle,
    balk: Balk,
};

discordClient.on("message", async msg => {
    if (!msg.content.startsWith(prefix) || msg.author.bot) {
        return;
    }

    const args = msg.content.slice(prefix.length).split(/ +/);
    const commandLabel = args.shift().toLowerCase();

    // Find command to call
    let command: Command = commandMap[commandLabel];

    // Make sure it isn't a hidden one called in the wrong channel
    if (command !== undefined) {
        if ((command.hidden() && msg.channel.id !== "593901709198098442" && msg.channel.id !== SNOWFLAKE_CHANNEL_UMP_PINGS && msg.channel.id !== SNOWFLAKE_CHANNEL_OOTC) && !DEVELOPMENT_MODE)
            return;
        if (command.nlCentral() && msg.channel.id !== SNOWFLAKE_CHANNEL_NL_CENTRAL)
            return;
    }

    // Now do nothing if we don't know the command
    if (command === undefined)
        return;

    try {
        // Call the command
        let response = await command.handleCommand(args, msg);
        if (response === undefined)
            return;

        // Append the "If something's wrong" footer if it's an Embed
        if (response instanceof RichEmbed) {
            response.footer = EMBED_FOOTER;
        }

        // Send the response
        msg.channel.send(response)
            .catch(console.error);
    } catch (e) {
        // DMs me (Llamos) when there's an unhandled error in a command on production.
        if (!DEVELOPMENT_MODE) {
            discordClient.guilds.get(SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN).members.get(SNOWFLAKE_USER_LLAMOS).send("An error in console for you, sir.").catch(console.error);
            console.error(e);
            msg.channel.send(`Something went wrong. I've already notified Llamos (the author), so please try again later.`)
                .catch(console.error);
        }
    }
});

// Login to Discord, starts background sockets
discordClient.login(DISCORD_TOKEN)
    .catch(console.error);

console.log(`Development Mode: ${DEVELOPMENT_MODE ? "Yes" : "No"}`);

discordClient.on("messageReactionAdd", async (messageReaction, user) => {
    // Make sure the bot wrote the message to be reacted to
    if (messageReaction.message.author.id === discordClient.user.id) {
        // For baseball reaction, should be .ping
        if (messageReaction.emoji.name === "⚾") {
            await commandMap.ping.handlePingReaction(messageReaction.message, user);
        }
    }
});

// if (!DEVELOPMENT_MODE)
//     setInterval(() => updateScoreboard().catch(console.error), 300000);

async function updateScoreboard() {
    let reqOptions = {
        uri: "https://redditball.xyz/api/v1/games/3/19",
        json: true,
    };

    let games = await rp(reqOptions);
    let embed = new RichEmbed();

    embed.setTitle("Playoffs Scoreboard");
    embed.setColor(0xffffff);
    embed.setAuthor("FakeBaseball", undefined, "https://redditball.xyz");
    embed.setTimestamp(new Date());
    embed.setFooter("Updated Automatically");

    { // NLCS
        let game = games.find(t => t.homeTeam.tag === "MTL");
        let title = `Paper Cup: ${game.awayTeam.tag} ${game.awayScore} - ${game.homeScore} ${game.homeTeam.tag}: ${game.completed ? "FINAL" : `${game.thirdOccupied ? "x" : "\\_"} ${game.secondOccupied ? "ˣ" : "-"} ${game.firstOccupied ? "x" : "\\_"} | ${game.inning} ${game.outs} Out`}`;
        let description = `[View Game](https://redditball.xyz/games/${game.id})`;
        embed.addField(title, description);
    }

    let message = await getBotSetting("scoreboard.message");
    let guild = discordClient.guilds.get(SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN);
    let channel = guild.channels.get(SNOWFLAKE_CHANNEL_SCOREBOARD) as TextChannel;
    if (message !== undefined) {
        let existingMessage: Message;
        try {
            existingMessage = await channel.fetchMessage(message);
        } catch (e) {
            await channel.bulkDelete(100, true);
            console.log("Couldn't find scoreboard message, recreating....");
        }
        if (existingMessage !== undefined) {
            await existingMessage.edit("", embed);
            return;
        }
    }

    let createdMessage: Message = await (discordClient.guilds.get(SNOWFLAKE_SERVER_FAKEBASEBALL_MAIN).channels.get(SNOWFLAKE_CHANNEL_SCOREBOARD) as TextChannel).send(embed) as Message;
    await setBotSetting("scoreboard.message", createdMessage.id);
}

if (!DEVELOPMENT_MODE)
    setInterval(() => updateFCBScoreboard().catch(console.error), 300000);

const FCB_TEAMS = ["CLM", "GT", "MIC", "NU", "SBU", "ULA", "UNC", "VAN", "VT", "WWU", "UWO", "WSU"];
async function updateFCBScoreboard() {
    let session = await getBotSetting("scoreboard.fcbsession");
    let reqOptions = {
        uri: `https://redditball.xyz/api/v1/games/4/${session}`,
        json: true,
    };

    let games = (await rp(reqOptions)).filter(g => FCB_TEAMS.includes(g.homeTeam.tag));
    let embed = new RichEmbed();

    embed.setTitle("FCB Scoreboard");
    embed.setColor(0xffffff);
    embed.setAuthor("FakeCollegeBaseball", undefined, "https://redditball.xyz");
    embed.setTimestamp(new Date());
    embed.setFooter("Updated Automatically");

    for (let game of games) {
        let title = `FCB: ${game.awayTeam.tag} ${game.awayScore} - ${game.homeScore} ${game.homeTeam.tag}: ${game.completed ? "FINAL" : `${game.thirdOccupied ? "x" : "\\_"} ${game.secondOccupied ? "ˣ" : "-"} ${game.firstOccupied ? "x" : "\\_"} | ${game.inning} ${game.outs} Out`}`;
        let description = `[View Game](https://redditball.xyz/games/${game.id})`;
        embed.addField(title, description);
    }

    let message = await getBotSetting("scoreboard.fcbmessage");
    let guild = discordClient.guilds.get("593511953952145418");
    let channel = guild.channels.get("595277577506127970") as TextChannel;
    if (message !== undefined) {
        let existingMessage: Message;
        try {
            existingMessage = await channel.fetchMessage(message);
        } catch (e) {
            await channel.bulkDelete(100, true);
            console.log("Couldn't find scoreboard message, recreating....");
        }
        if (existingMessage !== undefined) {
            await existingMessage.edit("", embed);
            return;
        }
    }

    let createdMessage: Message = await channel.send(embed) as Message;
    await setBotSetting("scoreboard.fcbmessage", createdMessage.id);
}
