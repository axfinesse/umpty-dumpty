create table if not exists games
(
	id int auto_increment,
	away_team varchar(10) not null,
	home_team varchar(10) not null,
	thread_link varchar(150) not null,
	created datetime default current_timestamp not null,
	primary key (id)
);
