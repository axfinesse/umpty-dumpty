CREATE TABLE IF NOT EXISTS settings (
    setting_user VARCHAR(64) NOT NULL,
    setting_key VARCHAR(128) NOT NULL,
    setting_value BOOL NOT NULL,
    PRIMARY KEY (setting_user, setting_key)
);
